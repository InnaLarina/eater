﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Статус лота
    /// </summary>
    public class Status
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public byte Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }
}

/*
Черновик
Размещён
Зарезервирован
Доставляется
Отменён
Завершён
*/