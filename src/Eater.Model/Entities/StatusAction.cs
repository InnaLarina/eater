﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Переход по действию из текущего статуса в следующий
    /// </summary>
    public class StatusAction
    {
        /// <summary>
        /// Текущий статус
        /// </summary>
        public Status CurrentStatus { get; set; }

        /// <summary>
        /// Действие
        /// </summary>
        public Action Action { get; set; }

        /// <summary>
        /// Следующий статус
        /// </summary>
        public Status NextStatus { get; set; }
    }
}