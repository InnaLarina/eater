﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class User
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        public byte[] Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public byte[] Password { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public UserType UserType { get; set; }

        /// <summary>
        /// Является волонтёром
        /// </summary>
        public bool IsVolunteer { get; set; }

        /// <summary>
        /// Роли
        /// </summary>
        public Role[] Roles { get; set; }

        /// <summary>
        /// Контакты
        /// </summary>
        public Contact[] Contacts { get; set; }
    }
}