﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Действие
    /// </summary>
    public class Action
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public byte Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }
}

/*
Создать
Изменить
Удалить
Разместить
Зарезервировать
Отменить
Доставить
Завершить
*/