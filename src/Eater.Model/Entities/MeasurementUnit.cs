﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Единица измерения
    /// </summary>
    public class MeasurementUnit
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public byte Id { get; set; }

        /// <summary>
        /// Название краткое
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }
}

/*
    Штука
    Упаковка
    Килограмм
    Литр
*/