﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Тип лота
    /// </summary>
    public class LotType
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public byte Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }
    }

    /*
    Полуфабрикат
    Готовая продукция
    */
}