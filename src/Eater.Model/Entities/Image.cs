﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Изображение
    /// </summary>
    public class Image
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Данные
        /// </summary>
        public byte[] Content { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }
}
