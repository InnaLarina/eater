﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Физическое лицо
    /// </summary>
    public class Person
    {
        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string MiddleName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Организация
        /// </summary>
        public Organization Organization { get; set; }
    }
}