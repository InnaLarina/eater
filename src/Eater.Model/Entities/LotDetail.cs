﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Состав лота
    /// </summary>
    public class LotDetail
    {
        /// <summary>
        /// Лот
        /// </summary>
        public Lot Lot { get; set; }

        /// <summary>
        /// Порядковый номер
        /// </summary>
        public short Item { get; set; }

        /// <summary>
        /// Категория продукта
        /// </summary>
        public ProductCategory ProductCategory { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Количество
        /// </summary>
        public int? Quantity { get; set; }

        /// <summary>
        /// Единица измерения
        /// </summary>
        public MeasurementUnit Unit { get; set; }

        /// <summary>
        /// Вес
        /// </summary>
        public decimal? Weight { get; set; }
    }
}