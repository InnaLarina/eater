﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Тип доставки
    /// </summary>
    public class DeliveryType
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public byte Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }
    }

    /*
    Доставка
    Самовывоз
    */
}
