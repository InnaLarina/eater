﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Изменение статуса лота
    /// </summary>
    public class StatusChange
    {
        /// <summary>
        /// Лот
        /// </summary>
        public Lot Lot { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Дата/время изменения
        /// </summary>
        public DateTime СhangeDateTime { get; set; }
    }
}
