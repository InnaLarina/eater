﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Контакт пользователя
    /// </summary>
    public class Contact
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public ContactType ContactType { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Значение по умолчанию (среди контактов одного типа)
        /// </summary>
        public bool ByDefault { get; set; }
    }

    /*
     User1
        Contacts

            Телефон     11111           0
            Телефон     22222222222     0
            Телефон     99999999999     1

            Адрес1     11111            1
            Адрес1     11111            0
            Адрес1     11111            0
            

     
     */

}