﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Тип контактных данных
    /// </summary>
    public class ContactType
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public byte Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }
    }

    /*
    Телефон
    E-Mail
    Адрес
    Геолокация
    */
}
