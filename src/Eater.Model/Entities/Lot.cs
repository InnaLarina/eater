﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Лот
    /// </summary>
    public class Lot
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Тип лота
        /// </summary>
        public LotType LotType { get; set; }

        /// <summary>
        /// Продавец
        /// </summary>
        public User Supplier { get; set; }

        /// <summary>
        /// Покупатель
        /// </summary>
        public User Customer { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Изображения
        /// </summary>
        public List<Image> Images { get; set; }
        
        /// <summary>
        /// Дата/время создания
        /// </summary>
        public DateTime CreationDateTime { get; set; }

        /// <summary>
        /// Срок годности
        /// </summary>
        public DateTime ExpireDate { get; set; }

        /// <summary>
        /// Срок резервирования
        /// </summary>
        public DateTime? ReserveDateTime { get; set; }

        /// <summary>
        /// Категория продукта
        /// </summary>
        public ProductCategory ProductCategory { get; set; }

        /// <summary>
        /// Состав лота
        /// </summary>
        public List<LotDetail> Details { get; set; }

        /// <summary>
        /// Тип доставки
        /// </summary>
        public DeliveryType DeliveryType { get; set; }

        /// <summary>
        /// Адрес доставки
        /// </summary>
        public string DeliveryAddress { get; set; }

        /// <summary>
        /// Вес
        /// </summary>
        public decimal? Weight { get; set; }

        /// <summary>
        /// Себестоимость
        /// </summary>
        public decimal? CostPrice { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public decimal? SalePrice { get; set; }

        /// <summary>
        /// Причина отмены
        /// </summary>
        public CancellationReason CancellationReason { get; set; }

        /// <summary>
        /// Дата/время завершения
        /// </summary>
        public DateTime? СompletionDateTime { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public Status Status { get; set; }
    }
}