﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Категория продукта
    /// </summary>
    public class ProductCategory
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public short Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Родительская категория
        /// </summary>
        public ProductCategory Parent { get; set; }
    }
}
