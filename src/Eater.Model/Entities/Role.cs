﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Роль системы
    /// </summary>
    public class Role
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public byte Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }

    /*
        Покупатель
        Продавец
        Администратор
    */
}
