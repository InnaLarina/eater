﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Причина отмены
    /// </summary>
    public class CancellationReason
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public byte Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }
    }

    /*
    Истёк срок годности
    Клиент отказался
    ...
    */
}
