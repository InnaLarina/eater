﻿namespace Eater.Model.Entities
{
    /// <summary>
    /// Тип пользователя
    /// </summary>
    public class UserType
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public byte Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }
    }

    /*
        Организация
        Физическое лицо
    */
}