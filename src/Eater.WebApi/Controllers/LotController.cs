﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Eater.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LotController : ControllerBase
    {
        private readonly ILogger<LotController> _logger;

        public LotController(ILogger<LotController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<LotController> Get()
        {
            return null;
        }
    }
}